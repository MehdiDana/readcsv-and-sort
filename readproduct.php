<?php
// Mehdi Dana - https://www.linkedin.com/in/mehdidana
/**
* To Run this, just make sure you have file products.csv in same directory as this file, then run 'php readproduct.php'
*/
$products = []; 
$row = 1;
if (($handle = fopen("products.csv", "r")) !== false) {

    while (($data = fgetcsv($handle, 1000, ",")) !== false) {
        $num = count($data);
        if (array_key_exists(trim($data[1]), $products)) {
        $products[trim($data[1])]['sizes'][trim($data[3])] = [
		    'SKU' => trim($data[0]),
		    'size' => trim($data[3]),
		    'sizeSort' => trim($data[4]),
	   	 ];
		} else {
			$products[trim($data[1])] = [
				'PLU' => trim($data[1]),
				'name' => trim($data[2]),
				'sizes' => [ trim($data[3]) =>
				[
					'SKU' => trim($data[0]),
					'size' => trim($data[3]),
					'sizeSort' => trim($data[4]),
				 ]
				]
			];
		}
	$row++;	
    }
    fclose($handle);
}

// UK shoe sizes
$sizeShoeUk = [
	'1 (Child)',
	'1.5 (Child)',
	'2 (child)',
	'2.5 (Child)',
	'3 (Child)',
	'3.5 (Child)',
	'4 (Child)',
	'4.5 (Child)',
	'5 (Child)',
	'5.5 (Child)',
	'6 (Child)',
	'6.5 (Child)',
	'7 (Child)',
	'7.5 (Child)',
	'8 (Child)',
	'8.5 (Child)',
	'9 (Child)',
	'9.5 (Child)',
	'10 (Child)',
	'10.5 (Child)',
	'11 (Child)',
	'11.5 (Child)',
	'12 (Child)',
	'1',
	'1.5',
	'2',
	'2.5',
	'3',
	'3.5',
	'4',
	'4.5',
	'5',
	'5.5',
	'6',
	'6.5',
	'7',
	'7.5',
	'8',
	'8.5',
	'9',
	'9.5',
	'10',
	'10.5',
	'11',
	'11.5',
	'12'
	];

// UK clothing sizes
$sizeClothingUk = [
	'XS',
	'S',
	'M',
	'L',
	'XL',
	'XXL',
	'XXXL',
	'XXXXL'
	];

// make sub array
foreach ($products as $index => $product) {
   if (array_values($product['sizes'])[0]['sizeSort'] == 'SHOE_UK') {
	// sort shoe uk
	$sizes = [];
	foreach ($sizeShoeUk as $size) {
	  if (array_key_exists($size, $product['sizes'])) {
	      $sizes[] = $product['sizes'][$size];
	  }
	}
	$products[$index]['sizes'] = array_values($sizes);
   } if (array_values($product['sizes'])[0]['sizeSort'] == 'CLOTHING_SHORT') {
	// sort clothing short
	$sizes = [];
	foreach ($sizeClothingUk as $size) {
	  if (array_key_exists($size, $product['sizes'])) {
	      $sizes[] = $product['sizes'][$size];
	  }
	}
	$products[$index]['sizes'] = array_values($sizes);
   }else {
   	ksort($product['sizes'], SORT_REGULAR);
	$products[$index]['sizes'] = array_values($product['sizes']);
   }

    // remove sub level keys
   foreach ($products[$index]['sizes'] as $indexSub => $sizesort) {
	unset($products[$index]['sizes'][$indexSub]['sizeSort']);
   }

}
$products= array_values($products);


var_dump(json_encode($products));




